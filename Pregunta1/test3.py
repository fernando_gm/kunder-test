"""
This is the first final code
"""

APERTURE = {
    '[': 1,
    '{': 2,
    '(': 3
}

CLOSE = {
    1: ']',
    2: '}',
    3: ')'
}

FIRST_OPERATORS = {
    '*':True,
    '/':True
}

LAST_OPERATORS = {
    '+':True,
    '-':True
}

def isOperator(char):
    return FIRST_OPERATORS.get(char, False) or LAST_OPERATORS.get(char, False)

def isFirstOperator(char):
    return FIRST_OPERATORS.get(char, False)

def isLastOperator(char):
    return LAST_OPERATORS.get(char, False)

def isAperture(char):
    return APERTURE.get(char, False)

def calculate(text):

    a = ''

    new_text = ''

    idx = 0

    while idx < len(text):

        char = text[idx]

        if isAperture(char):
            close_type = APERTURE.get(char, False)
            close_type = CLOSE[close_type] # ) 

            new_expression = text[idx+1:]
            new_expression = new_expression.split(close_type)[0]

            a = str(calculatemax(new_expression))
            idx += 1 + len(new_expression)
            new_text += a
        else:
            new_text += char
        
        idx += 1

    new_text = calculatemax(new_text)
    return new_text


def calculatemax(text):

    a = ''
    b = ''
    c = ''
    o = ''
    new_o = ''
    res = 0
    ev = False
    mult = False

    idx = 0

    while idx < len(text):


        char = text[idx]      

        if ev == False:
            if char.isnumeric()  or char == '.' or char == 'n':

                if char == 'n':
                    char = '-'
                a += char
            elif isOperator(char):
                o = char
                ev = True
                if isFirstOperator(char):
                    mult = True
                    new_o = char
                    b = a

            if isAperture(char):

                close_type = APERTURE.get(char, False)
                close_type = CLOSE[close_type] # ) 

                new_expression = text[idx+1:]
                new_expression = new_expression.split(close_type)[0]

                a = calculatemax(new_expression)

                idx += 2 + len(new_expression)
                ev = True

                char = text[idx]

                if char.isnumeric() or char == '.' or char == 'n':
                    pass
                    # Error
    
                elif isLastOperator(char):
                    o = char
                elif isFirstOperator(char):
                    if mult == False:
                        mult = True
                        new_o = char

                
        else:
            if isAperture(char):
                close_type = APERTURE.get(char, False)
                close_type = CLOSE[close_type] # ) 

                new_expression = text[idx+1:]
                new_expression = new_expression.split(close_type)[0]

                c = calculatemax(new_expression)
                
                hasB = True

                if b == '':
                    hasB = False

                idx += 2 + len(new_expression)
                mult = False

                if idx < (len(text) - 1):
                    char = text[idx]

                if hasB == True:
                    if new_o != '':
                        if new_o == '*':
                            b = float(b) * float(c)
                        elif new_o == '/':
                            b = float(b) / float(c)
                        new_o = ''
                    elif o != '':
                        if o == '+':
                            b = float(b) + float(c)
                        elif o == '-':
                            b = float(b) - float(c)
                    res = b
                else:
                    if new_o != '':
                        if new_o == '*':
                            a = float(a) * float(c)
                        elif new_o == '/':
                            a = float(a) / float(c)
                        new_o = ''
                    elif o != '':
                        if o == '+':
                            a = float(a) + float(c)
                        elif o == '-':
                            a = float(a) - float(c)
                    res = a
                
                
                c = ''

                if isOperator(char):
                    o = char
                    ev = True
                    if isFirstOperator(char):
                        mult = True
                        new_o = char

                if idx >= (len(text) - 1):
                    if a != '' and b != '':
                        if o == '+':
                            res = float(a) + float(b)
                        elif o == '-':
                            res = float(a) - float(b)

                
                
            
            elif mult == True:
                if char.isnumeric() or char == '.' or char == 'n':
                    if char == 'n':
                        char = '-'
                    c += char
                elif isOperator(char):
                    if new_o == '*':
                        b = float(b) * float(c)
                    elif new_o == '/':
                        b = float(b) / float(c)

                    if isLastOperator(char):
                        mult = False

                        if o == '+':
                            a = float(a) + float(b)
                        elif o == '-':
                            a = float(a) - float(b)
                        else:
                            a = b

                        b = ''
                        c = ''
                        o = char
                        
                    elif isFirstOperator(char):
                        new_o = char
                        c = ''

                if idx >= (len(text) - 1):

                    hasB = True

                    if b == '':
                        hasB = False

                    if new_o != '':
                        if hasB == True:
                            if new_o == '*':
                                b = float(b) * float(c)
                            elif new_o == '/':
                                b = float(b) / float(c)
                            new_o = ''
                            res = b
                        else:
                            if new_o == '*':
                                a = float(a) * float(c)
                            elif new_o == '/':
                                a = float(a) / float(c)
                            new_o = ''
                            res = a
                    elif o != '':
                        if o == '+':
                            res = float(a) + float(b)
                        elif o == '-':
                            res = float(a) - float(b)
                        o = '' 
                    
                    
            elif char.isnumeric() or char == '.' or char == 'n':
                if char == 'n':
                    char = '-'
                b += char
            elif isLastOperator(char):
                if o == '+':
                    a = float(a) + float(b)
                elif o == '-':
                    a = float(a) - float(b)
                o = char
                b = ''
                ev = True
            elif isFirstOperator(char):
                if mult == False:
                    mult = True
                    new_o = char

        if idx >= (len(text) - 1):
            if a != '' and b != '':
                if o == '+':
                    res = float(a) + float(b)
                elif o == '-':
                    res = float(a) - float(b)

        idx += 1

    res = str(res)
    res = res.replace('-', 'n')

    return res


if __name__ == '__main__':
    print(calculate('2+2')) # 4
    print(calculate('2-1+2-2+5+4')) # 10
    print(calculate('2+3*2/2+12')) # 17
    print(calculate('2+3*2+8/2')) # 12
    print(calculate('2*3+6')) # 12
    print(calculate('2*3+6/2*5')) # 21
    print(calculate('2*3+6-2*5-4/2')) # 0

    print('----------------------')
    print(calculate('[3+2*(3+2)+(3+4)]*(2+3-[2*2])')) # 20
    print(calculate('[3+2*(3+2)]*(3+3-[2*2])')) # 26
    print(calculate('(16+4-[3+3]+10-{2*2})/5-{5+8*(2+2)}*[1-2]')) # 41
    print(calculate('(16+4)/5-{5+8}*[1-2]')) # 17
    print(calculate('5*9-3+{2-1}*(9-[24/8])')) # 48
    print(calculate('[{(15-5)*5}/2]+(8-2)*{25+2}')) # 187