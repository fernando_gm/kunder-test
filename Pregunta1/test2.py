import requests

class Calculator:

    def __init__(self, expression):
        self.expression = expression.split('\n')
        self.APERTURE = {
            '[': 1,
            '{': 2,
            '(': 3
        }
        self.CLOSE = {
            1: ']',
            2: '}',
            3: ')'
        }
        self.FIRST_OPERATORS = {
            '*':True,
            '/':True
        }
        self.LAST_OPERATORS = {
            '+':True,
            '-':True
        }


    def __isOperator(self, char):
        return self.FIRST_OPERATORS.get(char, False) or self.LAST_OPERATORS.get(char, False)


    def __isFirstOperator(self, char):
        return self.FIRST_OPERATORS.get(char, False)


    def __isLastOperator(self, char):
        return self.LAST_OPERATORS.get(char, False)


    def __isAperture(self, char):
        return self.APERTURE.get(char, False)


    def evaluate(self):
        try:
            dig = ''

            res = ''

            res_list = []

            for expression in self.expression:

                expression = ''.join(expression.split(' '))

                idx = 0

                while idx < len(expression):

                    char = expression[idx]

                    if self.__isAperture(char):

                        new_expression = self.__splitExpression(expression, char, idx)

                        dig = str(self.__decompose(new_expression))
                        idx += 1 + len(new_expression)
                        res += dig
                    else:
                        res += char
                    
                    idx += 1

                res = self.__decompose(res)
                res_list.append(res)


                print('expression:',expression)
                print('result:', res)
                print()
            
                res = ''
            
        except ZeroDivisionError as e:
            print(e)
        except ValueError as e:
            print(e)
        except SyntaxError as e:
            print(e)
        except:
            print('Error')


    def __getOperation(self, operator, num_1, num_2, ):

        if operator == '*':
            return float(num_1) * float(num_2)
        elif operator == '/':
            return float(num_1) / float(num_2)
        elif operator == '+':
            return float(num_1) + float(num_2)
        elif operator == '-':
            return float(num_1) - float(num_2)


    def __splitExpression(self, expression, char, idx):
        close_type = self.APERTURE.get(char, False)
        close_type = self.CLOSE[close_type]

        new_expression = expression[idx+1:]
        
        if not close_type in new_expression:
            raise SyntaxError('Syntax Error')

        new_expression = new_expression.split(close_type)[0]

        return new_expression


    def __decompose(self, dig_expression):

        a = ''
        b = ''
        c = ''
        operator = ''
        new_operator = ''
        res = 0
        a_ready = False
        has_first_operator = False

        idx = 0

        while idx < len(dig_expression):

            char = dig_expression[idx]      

            if not a_ready:
                if char.isnumeric()  or char == '.' or char == 'n':

                    if char == 'n':
                        char = '-'
                    a += char
                elif self.__isOperator(char):
                    operator = char
                    a_ready = True
                    if self.__isFirstOperator(char):
                        has_first_operator = True
                        new_operator = char
                        b = a

                if self.__isAperture(char):

                    new_expression = self.__splitExpression(dig_expression, char, idx)

                    a = self.__decompose(new_expression)

                    idx += 2 + len(new_expression)
                    a_ready = True

                    char = dig_expression[idx]

                    if char.isnumeric() or char == '.' or char == 'n':
                        raise SyntaxError('Syntax Error')
        
                    elif self.__isLastOperator(char):
                        operator = char
                    elif self.__isFirstOperator(char):
                        if not has_first_operator:
                            has_first_operator = True
                            new_operator = char
        
            else:
                if self.__isAperture(char):
                    
                    new_expression = self.__splitExpression(dig_expression, char, idx)

                    c = self.__decompose(new_expression)
                    
                    has_b = True

                    if b == '':
                        has_b = False

                    idx += 2 + len(new_expression)
                    has_first_operator = False

                    if idx < (len(dig_expression) - 1):
                        char = dig_expression[idx]

                    if has_b:
                        if new_operator != '':
                            b = self.__getOperation(new_operator, b, c)
                            new_operator = ''
                        elif operator != '':
                            b = self.__getOperation(operator, b, c)
                        res = b
                    else:
                        if new_operator != '':
                            a = self.__getOperation(new_operator, a, c)
                            new_operator = ''
                        elif operator != '':
                            a = self.__getOperation(operator, a, c)
                        res = a
                    
                    c = ''

                    if self.__isOperator(char):
                        operator = char
                        if self.__isFirstOperator(char):
                            has_first_operator = True
                            new_operator = char

                    if idx >= (len(dig_expression) - 1):
                        if a != '' and b != '':
                            res = self.__getOperation(operator, a, b)
                
                elif has_first_operator:
                    if char.isnumeric() or char == '.' or char == 'n':
                        if char == 'n':
                            char = '-'
                        c += char
                    elif self.__isOperator(char):
                        b = self.__getOperation(new_operator, b, c)

                        if self.__isLastOperator(char):
                            has_first_operator = False

                            if self.__isLastOperator(operator):
                                a = self.__getOperation(operator, a, b)
                            else:
                                a = b

                            b = ''
                            c = ''
                            operator = char
                            
                        elif self.__isFirstOperator(char):
                            new_operator = char
                            c = ''

                    if idx >= (len(dig_expression) - 1):

                        has_b = True

                        if b == '':
                            has_b = False

                        if new_operator != '':
                            if has_b:
                                b = self.__getOperation(new_operator, b, c)
                                new_operator = ''
                                res = b
                            else:
                                a = self.__getOperation(new_operator, a, c)
                                new_operator = ''
                                res = a
                        elif operator != '':
                            res = self.__getOperation(operator, a, b)
                            operator = '' 
                               
                elif char.isnumeric() or char == '.' or char == 'n':
                    if char == 'n':
                        char = '-'
                    b += char
                elif self.__isLastOperator(char):
                    a = self.__getOperation(operator, a, b)
                    operator = char
                    b = ''
                elif self.__isFirstOperator(char):
                    if not has_first_operator:
                        has_first_operator = True
                        new_operator = char

            if idx >= (len(dig_expression) - 1):
                if a != '' and b != '':
                    if self.__isLastOperator(operator):
                        res = self.__getOperation(operator, a, b)

            idx += 1

        res = str(res)
        res = res.replace('-', 'n')

        return res

def main(uri):

    file = open(uri, 'r')

    expression = file.read()

    calculator = Calculator(expression)
    calculator.evaluate()


if __name__ == '__main__':

    main('expresionNumericaLista.txt')