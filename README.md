# Kunder Test

## Requeriments

python 3.x

## Test 1 Instructions

### Test files

The test is divided in three files, wich contains diferent implementations.

- **```test.py```**
  - This is the final test. It can read a file with a single math expression.
    - It returns the result so it can be printed or stored in a new variable.
- **```test2.py```**
  - Can read a file with multiple math expressions.
    - The expressions must be separated by line breaks.
    - It iterate the list of expressions and print each result.
- **```test3.py```**
  - This was the first code that works.
    - Do not read a file, the math expression must be added as parameter to a function.

### Run a test

- Open your terminal
- You must be located at ```/Pregunta1```
- Run ```python test.py```<span style="color:red">*</span>

\* ```python``` command must correspond to your python 3.x version.

### Modifying test

#### ```test.py```

If you want to add another file, just change the path text.

```py
# line 264

main('expresionNumerica.txt')

```

To change the original expression, open the file ```expresionNumerica.txt``` and change the content.

```txt
[{(15-5)*5}/2]+(8-2)*{25+2}
```

#### ```test2.py```

If you want to add another file, just change the path text.

```py
# line 297

main('expresionNumericaLista.txt')

```

To change the original expression list, open the file ```expresionNumericaList.txt``` and change the content.

```txt
(16+4)/5-{5+8}*[1-2]
5*9-3+{2-1}*(9-[24/8])
[{(15-5)*5}/2]+(8-2)*{25+2}
{3+5}*2/(4+4)+0*(-68+25
```

#### ```test3.py```

This file has some examples. You can add or remove them.

```py
# line 289

print(calculate('2+2')) # 4

```

## Test 2 Instructions

### Tests

- Python Test
  - This is a python file that you can run in your terminal
- Flask Test
  - Simple web aplication to consume kunder's api

### Run Tests

#### Python Test

- Open your terminal
- You must be located at ```/Pregunta2/python-test```
- Run ```python index.py```<span style="color:red">*</span>

\* ```python``` command must correspond to your python 3.x version.

#### Flask Test

- Open your terminal
- You must be located at ```/Pregunta2/flask-test```
- Run ```python index.py```<span style="color:red">*</span>
- Open your web browser and go to ```http://localhost:5000``` or ```http://127.0.0.1:5000```

\* ```python``` command must correspond to your python 3.x version.