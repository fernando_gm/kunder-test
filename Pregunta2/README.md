# Kunder Test 2

## Instructions

### Requeriments

python 3.x

### Tests

- Python Test
  - This is a python file that you can run in your terminal
- Flask Test
  - Simple web aplication to consume kunder's api

### Run Tests

#### Python Test

- Open your terminal
- You must be located at ```/Pregunta2/python-test```
- Run ```python index.py```<span style="color:red">*</span>

\* ```python``` command must correspond to your python 3.x version.

#### Flask Test

- Open your terminal
- You must be located at ```/Pregunta2/flask-test```
- Run ```python index.py```<span style="color:red">*</span>
- Open your web browser and go to ```http://localhost:5000``` or ```http://127.0.0.1:5000```

\* ```python``` command must correspond to your python 3.x version.
