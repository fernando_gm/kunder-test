import os
from flask import Flask, render_template, send_from_directory
import requests

app = Flask(__name__)


# Class
class Insurance:

    def __init__(self, insurances_api, contracted_api):
        """Constructor

        param str insurances_api : api uri
        param str contracted_api : api uri

        """

        self.insurances_api = insurances_api
        self.contracted_api = contracted_api
        self.top_five = []


    def getMostHiredInsuranceLast24Hrs(self, list_size):
        """
        returns list with top 5 contracted insurances
        """

        self.top_five = []

        insurances = requests.get(self.insurances_api)
        last_24h_contracted = requests.get(self.contracted_api)

        insurances = insurances.json()['insurance']

        insurances = sorted(insurances, key = lambda i: i['id'])

        last_24h_contracted = last_24h_contracted.json()['contracted']['results']

        contracted_counter = {}

        for x in last_24h_contracted:
            insuranceId = str(x['insuranceId'])

            exist = contracted_counter.get(insuranceId, False)

            if exist != False:
                contracted_counter[insuranceId] += 1
            else:
                contracted_counter[insuranceId] = 1

        contracted_counter = sorted(contracted_counter.items(), key=lambda x: x[1], reverse=True)

        if list_size > len(contracted_counter):
            list_size = len(contracted_counter)
        
        for x in range(list_size):
            insuranceId = contracted_counter[x][0]
            insurance = next(item for item in insurances if insuranceId == item['id'])
            insurance = {**insurance, 'contracted': contracted_counter[x][1], 'idx': x}
            self.top_five.append(insurance)
        
        return self.top_five



# Routes
@app.route('/')
def index():
    return render_template('index.html')

# Routes
@app.route('/top-most-hired/<top>')
def topMostHired(top):

    if not top.isnumeric():
        top = 0

    top = int(top)

    insurances_api = 'https://hack.kunderlabs.com/exam/insurance/api/insurance'
    contracted_api = 'https://hack.kunderlabs.com/exam/insurance/api/insurance/contracted/today'

    insurances = Insurance(insurances_api, contracted_api)

    name_list = {}

    data = insurances.getMostHiredInsuranceLast24Hrs(top)

    for idx, x in enumerate(data):
        separator = x['name'][::-1].split(' ', 1)
        name_list = { **name_list, idx: { 'name': separator[1][::-1] } }


    return render_template('topMostHired.html', data = data, name_list = name_list)

# Route for favicon
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    app.run(debug=True)
